---
author: cblte 2023
description: Rewritten from the 100+ Python challenging programming exercises" 
tags: Golang, programming, exercises, challenges 
---

# Intermediate Exercises

This document contains all the intermediate exercises. If you find an
error or a bug or something that could be written better, please get in
contact with me. Thanks.

## Exercise 006

Write a program that calculates and prints the value according to the
given formula:

Q = Square root of \[(2 \* C \* D)/H\]

Following are the fixed values of C and H:

- C is 50. H is 30.

- D is the variable whose values should be input to your program in a
  comma-separated sequence.

Example:

Let us assume the following comma separated input sequence is given to
the program: 100,150,180

The output of the program should be: 18,22,24

**Hint:** use math functions

- use `math.Sqrt` for the square root
- use `math.Round` for rounding a float operation

**Solution:** [006/exercise006.go](006/exercise006.go)

## Exercise 007

Write a program which takes 2 digits, X,Y as input and generates a
2-dimensional array. The element value in the i-th row and j-th column
of the array should be i\*j. Note: i=0,1.., X-1; j=0,1,¡­Y-1.

Example: Suppose the following inputs are given to the program: 3,5

Then, the output of the program should be:

**Hint:**  In case of input data being supplied to the question, it should be
assumed to be a console input in a comma-separated form.

**Solution:** [007/exercise007.go](007/exercise007.go)

## Exercise 008

Write a program that accepts a comma separated sequence of words as
input and prints the words in a comma-separated sequence after sorting
them alphabetically.

Suppose the following input is supplied to the program:

    without,hello,bag,world

Then, the output should be:

    bag,hello,without,world

**Hint:** In case of input data being supplied to the question, it should be
assumed to be a console input.

**Solution:** [008/exercise008.go](008/exercise008.go)

## Exercise 009

Write a program that accepts sequence of lines as input and prints the
lines after making all characters in the sentence capitalized.

Suppose the following input is supplied to the program:

    Hello world
    Practice makes perfect

Then, the output should be:

    HELLO WORLD
    PRACTICE MAKES PERFECT

**Hint:** In case of input data being supplied to the question, it should be
assumed to be a console input.

**Solution:** [009/exercise009.go](009/exercise009.go)

## Exercise 010

Write a program that accepts a sequence of whitespace separated words as
input and prints the words after removing all duplicate words and
sorting them alphanumerically.

Suppose the following input is supplied to the program:

    hello world and practice makes perfect and hello world again

Then, the output should be:

    again and hello makes perfect practice world

**Hint:** In case of input data being supplied to the question, it should be
assumed to be a console input. We use set container to remove duplicated
data automatically and then use sorted() to sort the data.

**Solution:** [010/exercise010.go](010/exercise010.go)

## Exercise 011

Write a program which accepts a sequence of comma separated 4 digit
binary numbers as its input and then check whether they are divisible by
5 or not. The numbers that are divisible by 5 are to be printed in a
comma separated sequence.

Example: 0100,0011,1010,1001

Then the output should be: 1010

Notes: Assume the data is input by console.

**Hint:** In case of input data being supplied to the question, it should be
assumed to be a console input.

**Solution:** [011/exercise011.go](011/exercise011.go)

## Exercise 012

Write a program, which will find all such numbers between 100 and 300
(both included) such that each digit of the number is an even number.
The numbers obtained should be printed in a comma-separated sequence on
a single line.

**Hint:** In case of input data being supplied to the question, it should be
assumed to be a console input.

**Solution:** [012/exercise012.go](012/exercise012.go)

## Exercise 013

Write a program that accepts a sentence and calculate the number of
letters and digits.

Suppose the following input is supplied to the program:

    hello world! 123

Then, the output should be:

    LETTERS 10
    DIGITS 3

**Hint:** In case of input data being supplied to the question, it should be assumed to be a console input.

**Solution:** [013/exercise013.go](013/exercise013.go)

## Exercise 14

Write a program that accepts a sentence and calculate the number of upper case letters and lower case letters.
Suppose the following input is supplied to the program:

    Hello world!

Then, the output should be:

    UPPER CASE 1
    LOWER CASE 9

**Hints:** In case of input data being supplied to the question, it should be assumed to be a console input.

**Solution:** [014/exercise014.go](014/exercise014.go)
