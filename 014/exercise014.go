package main

import (
	"fmt"
	"unicode"
)

// Ex014 count upper and lower case letters in a string
func Ex014(input string) string {
	uppercount := 0
	lowercount := 0

	for _, ch := range input {
		// if unicode.IsUpper(ch) {
		// 	uppercount++
		// } else if unicode.IsLower(ch) {
		// 	lowercount++
		// }
		switch {
		case unicode.IsUpper(ch):
			uppercount++
		case unicode.IsLower(ch):
			lowercount++
		}
	}
	return fmt.Sprintf("UPPER CASE %v - LOWER CASE %v", uppercount, lowercount)
}
