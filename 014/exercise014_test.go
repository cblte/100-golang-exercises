package main

import (
	"reflect"
	"testing"
)

// Write a program that accepts a sentence and calculate the number of letters and digits.

func TestEx014(t *testing.T) {

	input := "Hello world!"
	want := "UPPER CASE 1 - LOWER CASE 9"

	got := Ex014(input)

	if !reflect.DeepEqual(got, want) {
		t.Errorf("\nEx014() = %v\nwant    = %v", got, want)
	}

}
