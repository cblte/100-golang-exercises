---
author: cblte 2023
description: Rewritten from the 100+ Python challenging programming exercises" 
tags: Golang, programming, exercises, challenges 
---

# Beginner Exercises

This document contains all the beginner exercises. If you find an error
or a bug or something that could be written better, please get in
contact with me. Thanks.

## Exercise 001: Find numbers devisable by 7 but not by 5

Write a program which will find all such numbers which are divisible by
7 but are not a multiple of 5, between 2000 and 3200 (both included).
The numbers obtained should be printed in a comma-separated sequence on
a single line.

**Hint:** Consider using strconv and strings.Join

**Solution:** [001/exercise001.go](001/exercise001.go)

## Exercise 002: Compute factorial

Write a program which can compute the factorial of a given numbers. The
results should be printed in a comma-separated sequence on a single
line.

Suppose the following input is supplied to the program: `8`

Then, the output should be: `40320`

**Hint:** In case of input data being supplied to the question, it should be
assumed to be a console input.

**Solution:** [002/exercise002.go](002/exercise002.go)

## Exercise 003: Create a map with numbers squared

With a given integral number n, write a program to generate a map that
contains (i, i\*i) such that is an integral number between 1 and n (both
included), and then the program should print the map with representation
of the value

Suppose the following input is supplied to the program: `8`

Then, the output should be: `map[1:1 2:4 3:9 4:16 5:25 6:36 7:49 8:64]`

**Hint:** Use `make` for the map and `%v` verb for the output.

Solution: [003/exercise003.go](003/exercise003.go)

## Exercise 004: Create a slice from comma-seperated input string

Write a program which accepts a sequence of comma-separated numbers from
console and generate an slice out of them. Return the slice.

Suppose the following input is supplied to the program:
`34, 67, 55, 33, 12, 98`.

Then, the output should be: `[34 67 55 33 12 98]`

**Hint:** In case of input data being supplied to the question, it should be
assumed to be a console input. package `strings` has a split method.

**Solution:** [004/exercise004.go](004/exercise004.go)

## Exercise 005: Define a "class" (interface) which has at least two methods

Create a seperate file (module) which has at least two methods:

- ReadString: to read a string from console input

- PrintString: to return the string in upper case.

Also create a `main.go` file that acts as calling class.

**Hint:** Use a bufio Scanner.

- Use `bufio.NewScanner(os.Stdin)` to read in a full line of text.
- use `go run .` to execute considering all files in the directory

**Solution:** [005/exercise005.go](005/exercise005.go)
